# README #

### What is this repository for? ###

This is the code for the Lightning Training. It has components to show you the 5 most recently modified Accounts and a way to quickly create new Accounts.

Version 1.0

### How do I get set up? ###

* If you don’t have a Developer Org, create one.
    * https://developer.salesforce.com/signup
* Create at least 3 Accounts
* From setup, search for “Lightning Components” and click
* Check ”Enable Debug Mode”
* Enable Lightning Experience and switch to it
    * Go to setup, click Lightning Experience, scroll to bottom turn on Lightning and enable for all users.
* Click the cog from the home page and then “Edit Page”
* Under “Custom” set up the custom domain (If you already have the custom domain set up you won’t see this and therefore, don't need to do anything else)
    * Refresh the page until the "log in under new domain" shows up.
    * Log in under the new domain, then deploy to users
    * You’ll have to re-log in to the org. If you’re in chrome, it might not log you in. Try clearing the cache or use a different browser.
* Create the Lightning Components in your org with the same names as they exist in this repo, and copy and paste the code into the corresponding components/event.
* Once all of the components are created, you should be able to click on the "Edit Page" from the Home page and add the new custom components to the layout.


### Who do I talk to? ###

* Eide Bailly
* blpeterson@eidebailly.com
* nmortensen@eidebailly.com