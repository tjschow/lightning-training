public with sharing class AccountsQuickViewCntrl {
	public AccountsQuickViewCntrl() {

	}

    /**
     * Get the 5 most recent accounts.
     */
    @AuraEnabled
    public static Account[] getRecentAccountsLimit5Apxc() {
        return [SELECT Id, Name FROM Account ORDER BY LastModifiedDate DESC LIMIT 5];
    }

    /**
     * Create a new account based on JSON data.
     * @param  String jsonData      data being passed from the client
     * @return                      newly created account
     */
    @AuraEnabled
    public static Account createAccountApxc(Account acc) {
        System.debug(acc);
        insert acc;

        return acc;
    }

}