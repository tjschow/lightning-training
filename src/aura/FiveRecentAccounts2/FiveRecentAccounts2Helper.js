({
	getRecentAccountsLimit5 : function (component, event, helper) {
        var action = component.get("c.getRecentAccountsLimit5Apxc");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var responseVal = response.getReturnValue();
                component.set("v.accounts", responseVal);
            } else {
                console.log('Failed');
            }
        });

        $A.enqueueAction(action);
	},
})