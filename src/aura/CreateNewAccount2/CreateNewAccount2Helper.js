({
	createNewAcc : function (component, event, helper) {
        var data = {
                Name: component.find("accNameInput").get("v.value"),
                sobjectType: "Account",
            },
            action = component.get("c.createAccountApxc");

        action.setParams({acc : data});

        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                var account = response.getReturnValue();
                console.log(account);
            } else {
                console.log('failed');
            }
        });

        $A.enqueueAction(action);
	},
})