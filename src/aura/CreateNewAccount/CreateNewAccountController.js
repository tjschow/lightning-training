({
	handleAccCreation : function (component, event, helper) {
        helper.createNewAcc(component, event, helper);
	},
    addAccToCmp : function (component, event, helper) {
        var accounts = event.getParam("accounts");
        component.set("v.accounts", accounts);
    },
})