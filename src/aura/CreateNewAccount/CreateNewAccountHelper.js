({
	createNewAcc : function (component, event, helper) {
        var data = {
                Name: component.find("accNameInput").get("v.value"),
                sobjectType: "Account",
            },
            action = component.get("c.createAccountApxc");

        action.setParams({acc : data});

        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                var account = response.getReturnValue();
                helper.addAccountEvt(account, component, event);
            } else {
                console.log('failed');
            }
        });

        $A.enqueueAction(action);
	},
    addAccountEvt : function (account, component, event) {
        var accEvt = $A.get("e.c:AccountCreationEvt"),
            accounts = component.get("v.accounts");

        accounts.push(account);
        accEvt.setParams({"accounts": accounts});
        accEvt.fire();
    },
})