({
	getRecentAccountsLimit5 : function (component, event, helper) {
        var action = component.get("c.getRecentAccountsLimit5Apxc");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var responseVal = response.getReturnValue();
                helper.fireAccEvt(responseVal);
            } else {
                console.log('Failed');
            }
        });

        $A.enqueueAction(action);
	},
    fireAccEvt : function (accounts) {
        console.log(accounts);
        var accEvt = $A.get("e.c:AccountCreationEvt");
        accEvt.setParams({"accounts":accounts});
        accEvt.fire();
    },
})